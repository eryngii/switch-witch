"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.switchImages = switchImages;
exports.switchCase = switchCase;
exports.capture = capture;
exports.SwitchWitch = void 0;

var _fromEvent = require("rxjs/observable/fromEvent");

var _operators = require("rxjs/operators");

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function $$(selector) {
  return Array.prototype.slice.call(document.querySelectorAll(selector));
}

var SwitchWitch =
/*#__PURE__*/
function () {
  function SwitchWitch(breakpoint) {
    _classCallCheck(this, SwitchWitch);

    _defineProperty(this, "_breakpoint", void 0);

    this._breakpoint = breakpoint;
  }

  _createClass(SwitchWitch, [{
    key: "isMobile",
    value: function isMobile() {
      return window.innerWidth < this._breakpoint;
    }
  }, {
    key: "onLayoutChange",
    value: function onLayoutChange(callback) {
      var _this = this;

      (0, _fromEvent.fromEvent)(window, 'resize').pipe((0, _operators.scan)(function (acc) {
        return [acc[1], _this.isMobile()];
      }, [this.isMobile(), this.isMobile()]), (0, _operators.filter)(function (acc) {
        return acc[0] ? !acc[1] : acc[1];
      })).subscribe(function (acc) {
        return callback(acc[1]);
      });
    }
  }]);

  return SwitchWitch;
}();

exports.SwitchWitch = SwitchWitch;

function switchImages(isMobile) {
  ['_pc', '_sp'].map(function (suffix) {
    return "img[src*='".concat(suffix, "']");
  }).concat(['[style^="background-image:"]']).filter(function (selector) {
    return $$(selector).length !== 0;
  }).map(function (selector) {
    var isImgTag = /^img/.test(selector);
    return $$(selector).map(function (element) {
      var src = isImgTag ? element.getAttribute('src') || '' : element.getAttribute('style') || '';
      var currentSuffix = isMobile ? '_sp' : '_pc';
      var replacement = src.replace(/(_pc|_sp)\.(.+)$/, "".concat(currentSuffix, ".$2"));
      return element.setAttribute(isImgTag ? 'src' : 'style', replacement);
    });
  });
}

function switchCase(cases, expression) {
  var filtered = cases.filter(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 1),
        comparator = _ref2[0];

    return comparator(expression);
  });
  return filtered.length ? filtered[0][1] : false;
}

function capture(x) {
  var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function (x) {
    return x;
  };
  console.log(x);
  callback(x);
  return x;
}